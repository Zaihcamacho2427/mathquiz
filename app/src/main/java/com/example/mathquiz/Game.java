package com.example.mathquiz;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private List<Questions> questions;
    private int numberCorrect;
    private int numberIncorrect;
    private int totalQuestions;
    private int score;
    private Questions currentQuestions;

    public Game(){
        numberCorrect = 0;
        numberIncorrect = 0;
        totalQuestions = 0;
        score = 0;
       currentQuestions = new Questions(10);
       questions = new ArrayList<Questions>();

    }

    public void makeNewQuestion(){
        currentQuestions = new Questions(totalQuestions * 2 + 5);
        totalQuestions++;
        questions.add(currentQuestions);
    }

    public boolean checkAnswer(int submittedAnswer){
        boolean isCorrect;
        if (currentQuestions.getAnswer() == submittedAnswer){
            numberCorrect++;
            isCorrect = true;
        }
        else{
            numberIncorrect++;
            isCorrect = false;
        }
       score = numberCorrect - numberIncorrect;
        return isCorrect;
    }




    public List<Questions> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Questions> questions) {
        this.questions = questions;
    }

    public int getNumberCorrect() {
        return numberCorrect;
    }

    public void setNumberCorrect(int numberCorrect) {
        this.numberCorrect = numberCorrect;
    }

    public int getNumberIncorrect() {
        return numberIncorrect;
    }

    public void setNumberIncorrect(int numberIncorrect) {
        this.numberIncorrect = numberIncorrect;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Questions getCurrentQuestions() {
        return currentQuestions;
    }

    public void setCurrentQuestions(Questions currentQuestions) {
        this.currentQuestions = currentQuestions;
    }
}
