package com.example.mathquiz;

import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btn_start, btn_answer0, btn_answer1, btn_answer2, btn_answer3;
    TextView tv_score, tv_timer, tv_questions,tv_bottomMessage;
    ProgressBar p_timer;
    Game g = new Game();

    int secondsRemaining = 30;

    CountDownTimer timer = new CountDownTimer(30000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            secondsRemaining--;
            tv_timer.setText(Integer.toString(secondsRemaining));
            p_timer.setProgress(30 - secondsRemaining);
        }

        @Override
        public void onFinish() {
        btn_answer0.setEnabled(false);
        btn_answer1.setEnabled(false);
        btn_answer2.setEnabled(false);
        btn_answer3.setEnabled(false);

        tv_bottomMessage.setText("Time us up! " + g.getNumberCorrect() + "/" + (g.getTotalQuestions() -1));

        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                btn_start.setVisibility(View.VISIBLE);

            }
        }, 4000);

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_start = (Button) findViewById(R.id.btn_start);
        btn_answer0 = (Button) findViewById(R.id.btn_answer0);
        btn_answer1 = (Button) findViewById(R.id.btn_answer1);
        btn_answer2 = (Button) findViewById(R.id.btn_answer2);
        btn_answer3 = (Button) findViewById(R.id.btn_answer3);

        p_timer = findViewById(R.id.p_timer);

        tv_score = (TextView) findViewById(R.id.tv_score);
        tv_timer = findViewById(R.id.tv_timer);
        tv_questions = findViewById(R.id.tv_questions);
        tv_bottomMessage = findViewById(R.id.tv_bottomMessage);

        tv_timer.setText("0 seconds");
        tv_score.setText("0pts");
        tv_questions.setText("");
        tv_bottomMessage.setText("Press Go");
        p_timer.setProgress(0);

        View.OnClickListener startButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button start_button = (Button) v;

                start_button.setVisibility(View.INVISIBLE);
                secondsRemaining = 30;
                tv_score.setText("0 pts");
                g = new Game();
                nextTurn();
                timer.start();

            }
        };

        View.OnClickListener answerButtonCLickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button buttonClicked = (Button) v;

                int answerSelected = Integer.parseInt(buttonClicked.getText().toString());

                g.checkAnswer(answerSelected);
                tv_score.setText("Score: " + Integer.toString(g.getScore()));
                nextTurn();

            }
        };

        btn_start.setOnClickListener(startButtonClickListener);

        btn_answer0.setOnClickListener(answerButtonCLickListener);
        btn_answer1.setOnClickListener(answerButtonCLickListener);
        btn_answer2.setOnClickListener(answerButtonCLickListener);
        btn_answer3.setOnClickListener(answerButtonCLickListener);




    }

    private void nextTurn(){

        //create a new question
        //set text on answer buttons
        //enable answer button
        //start the timer

        g.makeNewQuestion();
        int [] answer = g.getCurrentQuestions().getAnswerArray();
        btn_answer0.setText(Integer.toString(answer[0]));
        btn_answer1.setText(Integer.toString(answer[1]));
        btn_answer2.setText(Integer.toString(answer[2]));
        btn_answer3.setText(Integer.toString(answer[3]));

        btn_answer0.setEnabled(true);
        btn_answer1.setEnabled(true);
        btn_answer2.setEnabled(true);
        btn_answer3.setEnabled(true);

        tv_questions.setText(g.getCurrentQuestions().getQuestionPhrase());

        tv_bottomMessage.setText(g.getNumberCorrect() + "/" + (g.getTotalQuestions() -1));


    }
}
